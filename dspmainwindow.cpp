#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{
    ui->setupUi(this);
}

DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}
